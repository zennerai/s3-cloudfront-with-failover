output "logs_bucket_id" {
  value       = aws_s3_bucket.logs.id
  description = "Logging Bucket ID"
}

output "logs_bucket_arn" {
  value       = aws_s3_bucket.logs.arn
  description = "Logging Bucket ARN"
}

output "primary_bucket_id" {
  value       = aws_s3_bucket.source.id
  description = "Primary Bucket"
}

output "primary_bucket_arn" {
  value       = aws_s3_bucket.source.arn
  description = "Primary Bucket"
}

output "failover_bucket_id" {
  value       = aws_s3_bucket.destination.id
  description = "Primary Bucket"
}

output "failover_bucket_arn" {
  value       = aws_s3_bucket.destination.arn
  description = "Primary Bucket"
}

output "cloudfront_distribution_id" {
  value       = aws_cloudfront_distribution.s3_distribution.id
  description = "Primary Bucket"
}

output "cloudfront_distribution_arn" {
  value       = aws_cloudfront_distribution.s3_distribution.arn
  description = "Primary Bucket"
}
