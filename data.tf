# Get route53 zone id for the parent domain
data "aws_route53_zone" "parent_domain" {
  name         = var.parent_domain
  private_zone = false
}
