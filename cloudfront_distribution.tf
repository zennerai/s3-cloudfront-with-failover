# Cloudfront headers
resource "aws_cloudfront_response_headers_policy" "security_headers_policy" {
  name = "${var.namespace}-${var.stage}-${var.project}-security-headers-policy"
  security_headers_config {
    content_type_options {
      override = true
    }
    frame_options {
      frame_option = "DENY"
      override     = true
    }
    referrer_policy {
      referrer_policy = "same-origin"
      override        = true
    }
    xss_protection {
      mode_block = true
      protection = true
      override   = true
    }
    # strict_transport_security {
    #   access_control_max_age_sec = "63072000"
    #   include_subdomains         = true
    #   preload                    = true
    #   override                   = true
    # }
    # content_security_policy {
    #   content_security_policy = "frame-ancestors 'none'; default-src 'none'; img-src 'self'; script-src 'self'; style-src 'self'; object-src 'none'"
    #   override                = true
    # }
  }
}

# The actual distribution
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin_group {
    origin_id = "${var.namespace}-${var.stage}-${var.project}-s3-group"

    failover_criteria {
      status_codes = [500, 502, 503, 504]
    }

    member {
      origin_id = "${var.namespace}-${var.stage}-${var.project}-primary"
    }

    member {
      origin_id = "${var.namespace}-${var.stage}-${var.project}-failover"
    }
  }

  tags = {
    ManagedBy  = "Terraform"
    Namespace  = var.namespace
    Stage      = var.stage
    Project    = var.project
    Attributes = "cdn"
    Name       = "${var.namespace}-${var.stage}-${var.project}-cdn"
  }

  origin {
    domain_name = aws_s3_bucket.source.bucket_regional_domain_name
    origin_id   = "${var.namespace}-${var.stage}-${var.project}-primary"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cloudfront_oai.cloudfront_access_identity_path
    }
  }

  origin {
    domain_name = aws_s3_bucket.destination.bucket_regional_domain_name
    origin_id   = "${var.namespace}-${var.stage}-${var.project}-failover"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cloudfront_oai.cloudfront_access_identity_path
    }
  }

  price_class = var.stage == "prd" ? "PriceClass_All" : "PriceClass_100"

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "${var.namespace}-${var.stage}-${var.project}"
  default_root_object = var.index_page

  custom_error_response {
    error_caching_min_ttl = 300
    error_code            = 404
    response_code         = 200
    response_page_path    = "/${var.error_page}"
  }

  custom_error_response {
    error_caching_min_ttl = 300
    error_code            = 403
    response_code         = 200
    response_page_path    = "/${var.error_page}"
  }

  logging_config {
    include_cookies = false
    bucket          = aws_s3_bucket.logs.bucket_domain_name
    prefix          = "cloudfront-logs"
  }

  aliases = var.alt_domain != null ? ["${var.domain}.${var.parent_domain}", var.alt_domain] : ["${var.domain}.${var.parent_domain}"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${var.namespace}-${var.stage}-${var.project}-s3-group"

    response_headers_policy_id = aws_cloudfront_response_headers_policy.security_headers_policy.id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = var.acm_certificate
    ssl_support_method  = "sni-only"
  }
}

# OAI
resource "aws_cloudfront_origin_access_identity" "cloudfront_oai" {
  comment = "OAI for cloudfront"
}


# Bucket policy for source bucket: OAI access
resource "aws_s3_bucket_policy" "source_bucket_policy" {
  bucket = aws_s3_bucket.source.id
  policy = <<POLICY
{
    "Version": "2008-10-17",
    "Id": "PolicyForCloudFrontPrivateContent",
    "Statement": [
        {
            "Sid": "1",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${aws_cloudfront_origin_access_identity.cloudfront_oai.iam_arn}"
            },
            "Action": "s3:GetObject",
            "Resource": "${aws_s3_bucket.source.arn}/*"
        }
    ]
}
POLICY
}


# Bucket policy for destination bucket: OAI access
resource "aws_s3_bucket_policy" "destination_bucket_policy" {
  provider = aws.alt_region
  bucket   = aws_s3_bucket.destination.id
  policy   = <<POLICY
{
    "Version": "2008-10-17",
    "Id": "PolicyForCloudFrontPrivateContent",
    "Statement": [
        {
            "Sid": "1",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${aws_cloudfront_origin_access_identity.cloudfront_oai.iam_arn}"
            },
            "Action": "s3:GetObject",
            "Resource": "${aws_s3_bucket.destination.arn}/*"
        }
    ]
}
POLICY
}