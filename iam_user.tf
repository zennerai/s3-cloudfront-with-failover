resource "aws_iam_user" "iam_user_cicd" {
  name          = "${var.namespace}-${var.stage}-${var.project}-cicd-user"
  path          = "/"
  force_destroy = true
}

resource "aws_iam_policy" "cicd_policy" {
  name = "${var.namespace}-${var.stage}-${var.project}-origin-access"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "S3RequiredAccess",
      "Action": [
        "s3:DeleteObject",
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:PutObject",
        "s3:getBucketVersioning"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.source.arn}/*"
    },
    {
        "Effect":"Allow",
        "Action": "s3:ListAllMyBuckets",
        "Resource":"*"
    },
    {
        "Effect":"Allow",
        "Action":["s3:ListBucket","s3:GetBucketLocation"],
        "Resource":"${aws_s3_bucket.source.arn}"
    },
    {
        "Sid": "CloudfrontPermissions",
        "Effect": "Allow",
        "Action": [
            "cloudfront:ListDistributions",
            "cloudfront:CreateInvalidation",
            "cloudfront:GetDistribution",
            "cloudfront:ListInvalidations",
            "cloudfront:GetInvalidation"
        ],
        "Resource": "${aws_cloudfront_distribution.s3_distribution.arn}"
    }
  ]
}
EOF
}

resource "aws_iam_user_policy_attachment" "attach_orgin_access_to_user" {
  user       = var.cicd_user != null ? var.cicd_user : aws_iam_user.iam_user_cicd.name
  policy_arn = aws_iam_policy.cicd_policy.arn
}