terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.70.0"
    }
  }
  required_version = "~> 1.1.1"

  backend "remote" {
    organization = "Zenner"

    workspaces {
      name = "test-cdn-1"
    }
  }
}

provider "aws" {
  region = var.region
}