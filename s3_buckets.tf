# provider for alt_region
provider "aws" {
  alias  = "alt_region"
  region = var.alt_region
}

# IAM role for cross-region replication
resource "aws_iam_role" "replication" {
  name               = "tf-iam-role-replication-${var.namespace}-${var.stage}-${var.project}"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}


# IAM policy for cross-region replication
resource "aws_iam_policy" "replication" {
  name   = "tf-iam-role-policy-replication-${var.namespace}-${var.stage}-${var.project}"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetReplicationConfiguration",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.source.arn}"
      ]
    },
    {
      "Action": [
        "s3:GetObjectVersionForReplication",
        "s3:GetObjectVersionAcl",
         "s3:GetObjectVersionTagging"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.source.arn}/*"
      ]
    },
    {
      "Action": [
        "s3:ReplicateObject",
        "s3:ReplicateDelete",
        "s3:ReplicateTags"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.destination.arn}/*"
    }
  ]
}
POLICY
}


# IAM policy attachment for cross-region replication
resource "aws_iam_role_policy_attachment" "replication" {
  role       = aws_iam_role.replication.name
  policy_arn = aws_iam_policy.replication.arn
}

# S3 bucket for logs
resource "aws_s3_bucket" "logs" {
  bucket        = "${var.namespace}-${var.stage}-${var.project}-logs"
  acl           = "private"
  force_destroy = true

  tags = {
    ManagedBy  = "Terraform"
    Namespace  = var.namespace
    Stage      = var.stage
    Project    = var.project
    Attributes = "logs"
    Name       = "${var.namespace}-${var.stage}-${var.project}-logs"
  }

  versioning {
    enabled = false
  }
}

# Destination/Replicated Bucket
resource "aws_s3_bucket" "destination" {
  provider      = aws.alt_region
  bucket        = "${var.namespace}-${var.stage}-${var.project}-failover-origin"
  acl           = "private"
  force_destroy = true

  tags = {
    ManagedBy  = "Terraform"
    Namespace  = var.namespace
    Stage      = var.stage
    Project    = var.project
    Attributes = "failover-origin"
    Name       = "${var.namespace}-${var.stage}-${var.project}-failover-origin"
  }

  versioning {
    enabled = true
  }
}

# Source S3 bucket
resource "aws_s3_bucket" "source" {
  bucket        = "${var.namespace}-${var.stage}-${var.project}-origin"
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }

  tags = {
    ManagedBy  = "Terraform"
    Namespace  = var.namespace
    Stage      = var.stage
    Project    = var.project
    Attributes = "origin"
    Name       = "${var.namespace}-${var.stage}-${var.project}-origin"
  }

  replication_configuration {
    role = aws_iam_role.replication.arn

    rules {
      id     = "replication-rule"
      status = "Enabled"

      filter {
        tags = {}
      }
      destination {
        bucket        = aws_s3_bucket.destination.arn
        storage_class = "STANDARD"

        replication_time {
          status  = "Enabled"
          minutes = 15
        }

        metrics {
          status  = "Enabled"
          minutes = 15
        }
      }
    }
  }
}

# Block public access for source bucket
resource "aws_s3_bucket_public_access_block" "block_source_bucket_public_access" {
  bucket                  = aws_s3_bucket.source.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# Block public access for logs bucket
resource "aws_s3_bucket_public_access_block" "block_cloudfront_logs_bucket_public_access" {
  bucket                  = aws_s3_bucket.logs.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# Block public access for destination/replicated bucket
resource "aws_s3_bucket_public_access_block" "block_destination_bucket_public_access" {
  provider                = aws.alt_region
  bucket                  = aws_s3_bucket.destination.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}