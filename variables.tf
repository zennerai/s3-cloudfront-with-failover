variable "region" {
  type        = string
  description = "AWS main region to deploy resources"
  default     = "us-east-1"
}

variable "alt_region" {
  type        = string
  description = "AWS alt region to deploy replication bucket"
  default     = "us-west-1"
}

variable "stage" {
  type        = string
  description = "stage name"
}

variable "project" {
  type        = string
  description = "Name of the project"
}

variable "namespace" {
  type        = string
  description = "namespace"
}

variable "index_page" {
  type        = string
  default     = "index.html"
  description = "Name of the index page"
}

variable "error_page" {
  type        = string
  default     = "index.html"
  description = "Name of the error page"
}

variable "acm_certificate" {
  type        = string
  description = "Arn of the ACM certificate"
}

variable "domain" {
  type        = string
  description = "Domain name to use with cloudfront"
}

variable "parent_domain" {
  type        = string
  description = "Parent domain name to use with cloudfront"
}

variable "alt_domain" {
  type        = string
  default     = null
  description = "Alternate domain name to use with cloudfront"
}

variable "cicd_user" {
  type        = string
  default     = null
  description = "Deploy user"
}