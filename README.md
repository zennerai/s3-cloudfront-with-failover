This module creates a cloudfront distributed, S3 backed website with a failover bucket in another region.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.1 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 3.70.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.70.0 |
| <a name="provider_aws.alt_region"></a> [aws.alt\_region](#provider\_aws.alt\_region) | 3.70.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudfront_distribution.s3_distribution](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/cloudfront_distribution) | resource |
| [aws_cloudfront_origin_access_identity.cloudfront_oai](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/cloudfront_origin_access_identity) | resource |
| [aws_iam_policy.cicd_policy](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/iam_policy) | resource |
| [aws_iam_policy.replication](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/iam_policy) | resource |
| [aws_iam_role.replication](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.replication](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_user.iam_user_cicd](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/iam_user) | resource |
| [aws_iam_user_policy_attachment.attach_orgin_access_to_user](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/iam_user_policy_attachment) | resource |
| [aws_route53_record.website_record](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/route53_record) | resource |
| [aws_s3_bucket.destination](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.logs](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.source](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_policy.destination_bucket_policy](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_policy.source_bucket_policy](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.block_cloudfront_logs_bucket_public_access](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.block_destination_bucket_public_access](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.block_source_bucket_public_access](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_route53_zone.parent_domain](https://registry.terraform.io/providers/hashicorp/aws/3.70.0/docs/data-sources/route53_zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_acm_certificate"></a> [acm\_certificate](#input\_acm\_certificate) | Arn of the ACM certificate | `string` | n/a | yes |
| <a name="input_alt_domain"></a> [alt\_domain](#input\_alt\_domain) | Alternate domain name to use with cloudfront | `string` | `null` | no |
| <a name="input_alt_region"></a> [alt\_region](#input\_alt\_region) | AWS alt region to deploy replication bucket | `string` | `"us-west-1"` | no |
| <a name="input_cicd_user"></a> [cicd\_user](#input\_cicd\_user) | Deploy user | `string` | `null` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | Domain name to use with cloudfront | `string` | n/a | yes |
| <a name="input_error_page"></a> [error\_page](#input\_error\_page) | Name of the error page | `string` | `"index.html"` | no |
| <a name="input_index_page"></a> [index\_page](#input\_index\_page) | Name of the index page | `string` | `"index.html"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | namespace | `string` | n/a | yes |
| <a name="input_parent_domain"></a> [parent\_domain](#input\_parent\_domain) | Parent domain name to use with cloudfront | `string` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | Name of the project | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS main region to deploy resources | `string` | `"us-east-1"` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | stage name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudfront_distribution_arn"></a> [cloudfront\_distribution\_arn](#output\_cloudfront\_distribution\_arn) | Primary Bucket |
| <a name="output_cloudfront_distribution_id"></a> [cloudfront\_distribution\_id](#output\_cloudfront\_distribution\_id) | Primary Bucket |
| <a name="output_failover_bucket_arn"></a> [failover\_bucket\_arn](#output\_failover\_bucket\_arn) | Primary Bucket |
| <a name="output_failover_bucket_id"></a> [failover\_bucket\_id](#output\_failover\_bucket\_id) | Primary Bucket |
| <a name="output_logs_bucket_arn"></a> [logs\_bucket\_arn](#output\_logs\_bucket\_arn) | Logging Bucket ARN |
| <a name="output_logs_bucket_id"></a> [logs\_bucket\_id](#output\_logs\_bucket\_id) | Logging Bucket ID |
| <a name="output_primary_bucket_arn"></a> [primary\_bucket\_arn](#output\_primary\_bucket\_arn) | Primary Bucket |
| <a name="output_primary_bucket_id"></a> [primary\_bucket\_id](#output\_primary\_bucket\_id) | Primary Bucket |
<!-- END_TF_DOCS -->